package br.com.trama.chart.util;

import java.util.Comparator;

import br.com.trama.chart.gantt.GanttTask;

/**
 * @author       $h@rk
 * @Date         21/06/2011
 * @project      jotdown-web
 * @Description: Comparator criado para odernar tarefas
 *               por data de execu��o inicial
 */
public class TaskDateComparator implements Comparator<GanttTask>{
	
	/**
	 * Compara datas de duas tarefas e determina qual a maior/menor
	 */
	public int compare(GanttTask a, GanttTask b) {

		//se retornar zero datas s�o iguais
		int result = 0;

		//datas iguais
		if(a.getPlannedStartDate().before(b.getPlannedStartDate())){
			result = -1;

			//data inicial superior a final
		} else if(a.getPlannedStartDate().after(b.getPlannedStartDate())){
			result = 1;
		}

		return result;
	}
}
