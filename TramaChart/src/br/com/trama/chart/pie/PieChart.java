package br.com.trama.chart.pie;
import java.util.ArrayList;
import java.util.List;

import br.com.trama.chart.Chart;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias(value = "graph")
public class PieChart implements Chart{
	
	@XStreamAsAttribute
	private String caption;
	
	@XStreamImplicit
	private List<PieChartSet> labels = new ArrayList<PieChartSet>();

	public PieChart(String caption) {
		this.caption = caption;		
	}

	public void add(PieChartSet pieChartSet) {
		labels.add(pieChartSet);
	}

	public String getCaption() {
		return caption;
	}

	@Override
	public int getX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getY() {
		// TODO Auto-generated method stub
		return 0;
	}
}