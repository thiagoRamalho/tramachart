package br.com.trama.chart.pie;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias(value = "set")
public class PieChartSet {
	@XStreamAsAttribute
	private String label;
	
	@XStreamAsAttribute
	private String name;
	
	@XStreamAsAttribute
	private String value;
	
	public PieChartSet(String label, String name, String value) {
		this.label = label;
		this.name = name;
		this.value = value;
	}
	
	public String getLabel() {
		return label;
	}
	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}
}