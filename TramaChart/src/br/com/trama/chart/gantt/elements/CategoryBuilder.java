package br.com.trama.chart.gantt.elements;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import br.com.trama.util.date.DateUtil;

/**
 * @author       $h@rk
 * @Date         25/06/2011
 * @project      TramaChart
 * @Description: Objeto construtor das categorias do gr�fico
 */
public class CategoryBuilder {	
	
	private final int ADD_ONE = 1;
	private SimpleDateFormat sdf;
	private GanttChart ganttChart;
	private Locale locale;
	
	private Calendar startProject;
	private Calendar endProject;
	
	public CategoryBuilder(Calendar startProject,  Calendar endProject, SimpleDateFormat sdf, GanttChart ganttChart, Locale locale){
		this.startProject = startProject;
		this.endProject = endProject;
		this.sdf = sdf;
		this.ganttChart = ganttChart;
		this.locale = locale;
	}
	
	/**
	 * Cria categoria ano
	 */
	public void createYearCategory() {
				
		Calendar startDate = this.getStartProjectDate();
		Calendar endDate   = this.getEndProjectDate();
		
		//iteramos at� que os anos se equivalham
		while(startDate.get(Calendar.YEAR) < endDate.get(Calendar.YEAR)){
			
			//setamos a data final sempre como o �ltimo dia do ano
			Calendar endDateOfYear = new GregorianCalendar(startDate.get(Calendar.YEAR), Calendar.DECEMBER, 31);
			DateUtil.getLastDayOfMonth(endDateOfYear);			
			this.setYear(startDate, endDateOfYear);
			
			//resetamos a data
			startDate.set(startDate.get(Calendar.YEAR), Calendar.JANUARY, 01);
						
			//adicionamos um ano a data
			DateUtil.addYear(startDate, ADD_ONE);
		}
		
		//realiza processo novamente para que o �ltimo ano
		//do projeto seja contabilizado com a data final do
		//projeto
		this.setYear(startDate, endDate);
	}

	/**
	 * Seta o ano configurando datas de in�cio e fim
	 * 
	 * @param startDate
	 * @param endDateOfYear
	 */
	private void setYear(Calendar startDate, Calendar endDateOfYear) {
		
		//pegamos as datas com base no pattern
		String start = DateUtil.CalendarToStr(this.sdf, startDate);
		String end = DateUtil.CalendarToStr(this.sdf, endDateOfYear); 
		
		//pegamos o nome do m�s 
		String name = ""+startDate.get(Calendar.YEAR);
		
		//cria categoria com base nas informa��es extra�das
		this.ganttChart.addYearCategory(new CategoryChartElement(start, end, name));
	}


	/**
	 * Cria categoria m�s
	 * 
	 * Obs.: Espera lista de tarefas ordenadas
	 * 
	 * @param projeto
	 * @return
	 */
	public void createMonthCategory() {
		
		//pega a primeira e �ltima tarefas para que seja poss�vel acessar as datas 
		//que representam o in�cio e fim do projeto
		Calendar startDate = this.getStartProjectDate();
		Calendar endDate   = this.getEndProjectDate();
		
		//iteramos at� que o m�s/ano se equivalham
		while(!this.compareMonthYear(startDate, endDate)){
			
			//clonamos a data inicial novamente pois precisamos
			//descobrir o limite de dias do m�s
			Calendar lastDayOfStartDate = (Calendar) startDate.clone();			
			DateUtil.getLastDayOfMonth(lastDayOfStartDate);
			
			this.setMonth(startDate, lastDayOfStartDate);						
			
			//setamos limites dos dias para o m�s
			DateUtil.getFirstDayOfMonth(startDate);
			
			//adicionamos um m�s a data inicial para que seja 
			//poss�vel itera��o pelo pr�ximo m�s
			DateUtil.addMonth(startDate, this.ADD_ONE);
		}
		
		//refaz o processo por�m considera a data final do 
		//projeto
		
		//clonamos a data inicial novamente pois precisamos
		//descobrir o limite de dias do m�s
		Calendar lastDayOfProject = (Calendar) endDate.clone();
		this.setMonth(startDate, lastDayOfProject);
	}

	/**
	 * Seta o m�s no gr�fico ajustando dias de in�cio e fim
	 * @param startDate
	 * @param lastDayOfStartDate
	 */
	private void setMonth(Calendar startDate, Calendar lastDayOfStartDate) {
				
		//pegamos as datas com base no pattern
		String start = DateUtil.CalendarToStr(this.sdf, startDate);
		String end   = DateUtil.CalendarToStr(this.sdf, lastDayOfStartDate);
		
		//pegamos o nome do m�s 
		String name = startDate.getDisplayName(Calendar.MONTH, Calendar.SHORT, this.locale);
				
		//cria categoria com base nas informa��es extra�das
		this.ganttChart.addMonthCategory(new CategoryChartElement(start, end, name));
	}
	
	/**
	 * Cria categoria dias
	 */
	public void createDayCategory() {
		
		//pega a primeira e �ltima tarefas para que seja poss�vel acessar as datas 
		//que representam o in�cio e fim do projeto
		Calendar startDate = this.getStartProjectDate();
		Calendar endDate   = this.getEndProjectDate();
		
		//iteramos criando a categoria dias com base nos dias utilizados
		while(!startAfterFinish(startDate, endDate)){			
			
			//pegamos as datas com base no pattern
			String start = DateUtil.CalendarToStr(this.sdf, startDate);			
			String end   = DateUtil.CalendarToStr(this.sdf, startDate);
			
			//adiciona zero a esquerda se necess�rio
			String name  = String.format("%02d", startDate.get(Calendar.DAY_OF_MONTH));
			
			//cria categoria com base nas informa��es extra�das
			this.ganttChart.addDayCategory(new CategoryChartElement(start, end, name));						
			
			//adicionamos um dia a data inicial para que seja 
			DateUtil.addDays(startDate, this.ADD_ONE);
		}		
	}
	
	
	/**
	 * Recupera a data inicial do projeto
	 * 
	 * @return
	 */
	private Calendar getStartProjectDate() {
		return(Calendar) this.startProject.clone();
	}
	
	/**
	 * Recupera a data final do projeto
	 * @return
	 */
	private Calendar getEndProjectDate() {
		return (Calendar) this.endProject.clone();
	}




	/**
	 * Verifica se data inicial � superior a final
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	private boolean startAfterFinish(Calendar startDate, Calendar endDate){
		
		//clona as data pois � necess�rio modificar as horas
		Calendar start = (Calendar) startDate.clone();
		Calendar end   = (Calendar) endDate.clone();
		
		//igualamos as horas para n�o haver erros
		//de compara��o
		DateUtil.suppressHours(startDate);
		DateUtil.suppressHours(endDate);
		
		return start.after(end);
	}

	/**
	 * Compara duas datas levando-se em conta
	 * apenas m�s e ano
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	private boolean compareMonthYear(Calendar startDate, Calendar endDate){
		
		boolean validMonth  = startDate.get(Calendar.MONTH) ==  endDate.get(Calendar.MONTH);
		boolean equalsYear  = startDate.get(Calendar.YEAR)  == endDate.get(Calendar.YEAR);
		
		return (equalsYear && validMonth);
	}	
}
