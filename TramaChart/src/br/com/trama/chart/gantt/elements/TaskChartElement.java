package br.com.trama.chart.gantt.elements;


import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * @author       $h@rk
 * @Date         20/06/2011
 * @project      jotdown-web
 * @Description: Classe que representa uma tarefa no gr�fico de Gantt
 */
@XStreamAlias("task")
class TaskChartElement {
	
	@XStreamAsAttribute
	private String start;
	@XStreamAsAttribute
	private String end;
	@XStreamAsAttribute
	private Long processId;
	@XStreamAsAttribute
	private String name;
	
	@XStreamAsAttribute
	@XStreamAlias("showName")
	private String isShowName;
	
	@XStreamAsAttribute
	private String animation;
	
	
	/**
	 * Construtor �nico para setar atributos 
	 * 
	 * @param start      Data inicial executa��o (pattern + HH:MM:SS)
	 * @param end        Data final  executa��o  (pattern + HH:MM:SS)
	 * @param processId  id do elemento process para cruzamento
	 * @param name       nome da tarefa
	 * @param isShowName determina se nome vai aparecer ao lado da barra
	 *                   de exibi��o da tarefa
	 */
	TaskChartElement(String start, String end, Long processId, String name, boolean isShowName) {
		this.start = start;
		this.end = end;
		this.processId = processId;
		this.name = name;
		this.isShowName = isShowName ? "1":"0";
		this.animation = "0";
	}

	String getStart() {
		return start;
	}
	String getEnd() {
		return end;
	}
	long getProcessId() {
		return processId;
	}
	String getName() {
		return name;
	}

	String isShowName() {
		return isShowName;
	}

	public String getAnimation() {
		return animation;
	}
	
	
}
