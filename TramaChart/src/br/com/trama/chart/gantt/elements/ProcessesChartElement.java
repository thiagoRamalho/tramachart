package br.com.trama.chart.gantt.elements;


import java.util.ArrayList;
import java.util.List;

import br.com.trama.validation.Validation;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * @author       $h@rk
 * @Date         20/06/2011
 * @project      jotdown-web
 * @Description: Um n� de processos, essa classe foi criada pois o n�
 *               possui caracter�sticas quem devem ser setadas
 *               (utilizar com gr�ficos de gantt)
 */

@XStreamAlias("processes")
class ProcessesChartElement {
	
	@XStreamAsAttribute
	private int fontSize;
	@XStreamAsAttribute
	private String isBold;
	@XStreamAsAttribute
	private String align;
	@XStreamAsAttribute
	private String headerText;
	@XStreamAsAttribute
	private int headerFontSize;
	@XStreamAsAttribute
	private String headerVAlign;
	@XStreamAsAttribute
	private String headerAlign;
	
	@XStreamImplicit
	private List<ProcessChartElement> processList;
	
	/**
	 * Construtor com par�metros de configura��o de caracteristicas
	 * dos process do gr�fico
	 * 
	 * @param fontSize       tamanho da fonte (processos)
	 * @param isBold         negrito
	 * @param align          alinhamento do texto (para processos)
	 * @param headerText     texto cabe�alho
	 * @param headerFontSize tamanho da fonte (cabe�alho)
	 * @param headerVAlign   alinhamento do texto op��o 1
	 * @param headerAlign    alinhamento do texto op��o 2
	 * 
	 * os alinhamentos de cabe�alho (dois �ltimos par�metros) devem ser combinados
	 * ex: top and right (posiciona cabe�alho acima e a direita)
	 * 
	 */
	ProcessesChartElement(int fontSize, boolean isBold, String align, String headerText, int headerFontSize, 
			         String headerVAlign, String headerAlign) {
		
		this.fontSize = fontSize;
		this.isBold = isBold ? "1" : "0";
		this.align = align;
		this.headerText = headerText;
		this.headerFontSize = headerFontSize;
		this.headerVAlign = headerVAlign;
		this.headerAlign = headerAlign;
		this.processList = new ArrayList<ProcessChartElement>();
	}
	
	/**
	 * Cria n� de processos setando valores padr�o
	 */
	public ProcessesChartElement(String headerText) {
		this.fontSize = 11;
		this.isBold = "1";
		this.align = "left";
		this.headerText = headerText;
		this.headerFontSize = 16;
		this.headerVAlign = "center";
		this.headerAlign  = "center";
		this.processList = new ArrayList<ProcessChartElement>();
	}
	
	/**
	 * Adiciona um novo processo ao n� validando
	 * o mesmo
	 * 
	 * @param process
	 */
	public void addProcess(ProcessChartElement process){		
		//verifica se objeto � nulo
		if(Validation.isValid(process)){
			this.processList.add(process);			
		}
	}

	public int getFontSize() {
		return fontSize;
	}

	public String isBold() {
		return isBold;
	}

	public String getAlign() {
		return align;
	}

	public String getHeaderText() {
		return headerText;
	}

	public int getHeaderFontSize() {
		return headerFontSize;
	}

	public String getHeaderVAlign() {
		return headerVAlign;
	}

	public String getHeaderAlign() {
		return headerAlign;
	}
	
	protected int processSize(){
		return this.processList.size();
	}
}
