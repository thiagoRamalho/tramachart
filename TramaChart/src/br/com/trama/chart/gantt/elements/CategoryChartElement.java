package br.com.trama.chart.gantt.elements;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;



/**
 * @author       $h@rk
 * @Date         19/06/2011
 * @project      jotdown-web
 * @Description: Classe que representa uma categoria de um gr�fico de 
 *               gantt
 */
@XStreamAlias("category")
public class CategoryChartElement {
	
	@XStreamAsAttribute
	private String start;
	@XStreamAsAttribute
	private String end;
	@XStreamAsAttribute
	private String name;
	
	/**
	 * Construtor �nico que representa uma categoria
	 * do gr�fico
	 * 
	 * @param start Data inicial (pattern + HH:MM:SS)
	 * @param end   data final   (pattern + HH:MM:SS)
	 * @param name  nome de exibi��o da categoria
	 * 
	 * Obs.: Os campos data representam o range em que
	 *       a categoria se situa 
	 * Ex: Janeiro de 01/01 � 28/01
	 * 
	 */
	public CategoryChartElement(String start, String end, String name) {
		this.start = start;
		this.end = end;
		this.name = name;
	}
	
	public String getStart() {
		return start;
	}
	
	public String getEnd() {
		return end;
	}
	public String getName() {
		return name;
	}
}
