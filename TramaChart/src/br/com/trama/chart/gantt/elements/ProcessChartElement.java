package br.com.trama.chart.gantt.elements;


import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * @author       $h@rk
 * @Date         19/06/2011
 * @project      jotdown-web
 * @Description: Elemento do gr�fico de gantt
 */
@XStreamAlias("process")
class ProcessChartElement {

	@XStreamAsAttribute
	private String name;
	
	@XStreamAsAttribute
	private long id;
	
	ProcessChartElement(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public long getId() {
		return id;
	}
	public String getName() {
		return name;
	}	
}
