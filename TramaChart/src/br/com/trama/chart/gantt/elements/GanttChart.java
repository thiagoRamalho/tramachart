package br.com.trama.chart.gantt.elements;


import java.util.ArrayList;
import java.util.List;

import br.com.trama.chart.Chart;
import br.com.trama.validation.Validation;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
/**
 * @author       $h@rk
 * @Date         19/06/2011
 * @project      jotdown-web
 * @Description: Classe que possui os elementos para gera��o de 
 *               gr�ficos de gantt
 */
@XStreamAlias("chart")
public class GanttChart implements Chart{
	
   	@XStreamAsAttribute
	private String dateFormat;

   	@XStreamAsAttribute
	private String ganttWidthPercent = "85";
	
   	@XStreamAlias("categories")
	private List<CategoryChartElement> yearList;	
   	
   	@XStreamAlias("categories")
	private List<CategoryChartElement> monthList;
   	
   	@XStreamAlias("categories")
	private List<CategoryChartElement> dayList;	
	
   	private ProcessesChartElement processes;
	
	@XStreamAlias("tasks")
	private List<TaskChartElement> taskList;	
	
	private List<LineChartElement> trendLines;
	
	/**
	 * 
	 * @param dateFormat pattern de formata��o das datas
	 * @param processes  objeto que cont�m os processos 
	 *                   que ser�o apresentados no gr�fico
	 */
	public GanttChart(String dateFormat, ProcessesChartElement processes){
		this.processes  = processes;
		this.dateFormat = dateFormat;
		this.startLists();
	}

	//construtor utilizado para testes
	public GanttChart() {
		this.startLists();
	}
	
	private void startLists() {
		this.yearList   = new ArrayList<CategoryChartElement>();
		this.monthList  = new ArrayList<CategoryChartElement>();
		this.dayList    = new ArrayList<CategoryChartElement>();
		this.taskList   = new ArrayList<TaskChartElement>();
	}
	
	/**
	 * Adiciona elementos a categoria ano
	 * 
	 * @param category
	 */
	public void addYearCategory(CategoryChartElement category){
		if(Validation.isValid(category)){
			this.yearList.add(category);			
		}
	}
	
	/**
	 * Adiciona elementos a categoria m�s
	 * 
	 * @param category
	 */
	public void addMonthCategory(CategoryChartElement category){
		if(Validation.isValid(category)){
			this.monthList.add(category);			
		}
	}
	
	/**
	 * Adiciona elementos a sub-categoria
	 * 
	 * @param category
	 */
	public void addDayCategory(CategoryChartElement category){
		if(Validation.isValid(category)){
			this.dayList.add(category);			
		}
	}
	
	/**
	 * Adiciona uma tarefa no gr�fico
	 * @param task
	 */
	public void addTask(TaskChartElement task){		
		//valida se � nulo
		if(Validation.isValid(task)){
			this.taskList.add(task);
		}
	}
	
	/**
	 * Adiciona linha ao trendLines
	 * 
	 * @param line
	 */
	public void addLines(LineChartElement line){
		if(Validation.isValid(line)){
			this.trendLines.add(line);
		}
	}	
	
	public ProcessesChartElement getProcesses() {
		return this.processes;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	@Override
	public int getX() {
		//setamos o percentual para o gantt
		//com base no n�mero de colunas
		return this.dayList.size();
	}

	@Override
	public int getY() {
		return this.processes.processSize();
	}
	
	//comentar m�todo ap�s os testes
	protected List<CategoryChartElement> getMonthList() {
		return monthList;
	}

	public void setChartPercent() {
/*		int xAxis = this.getXAxis();
		
		int width = xAxis *23;
		
		double percent = 100/width;
		
		this.ganttWidthPercent = String.valueOf((100 - percent));
*/	}

	public String getGanttWidthPercent() {
		return ganttWidthPercent;
	}
}
