package br.com.trama.chart.gantt.elements;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;

import br.com.trama.chart.Chart;
import br.com.trama.chart.gantt.GanttProject;
import br.com.trama.chart.gantt.GanttTask;
import br.com.trama.chart.util.TaskDateComparator;
import br.com.trama.util.date.DateUtil;
import br.com.trama.validation.Validation;

/**
 * 
 * @author       $h@rk
 * @Date         23/06/2011
 * @project      TramaChart
 * @Description: Constr�i objeto que representa gr�fico de gantt
 */
public class GanttBuilder {
	
	private String patternDate = "";
	private SimpleDateFormat sdf;
	
	private GanttProject project; 
	private Locale locale;
	private GanttChart gantt;
	
	private Calendar endProject;
	private Calendar startProject;
	
	public GanttBuilder(GanttProject ganttProject, Locale locale){
		this.project = ganttProject;
		this.locale = locale;
	}
	
	/**
	 * Constr�i objeto que representa o gr�fico
	 * @return
	 */
	public Chart createChart(){
		
		//lan�a exce��o caso n�o possua
		//elementos de cria��o do objeto
		this.verifyInfos();

		//configura elementos para cria��o do objeto
		this.setupGeneralInformation();
		
		//objeto com lista de processos
		ProcessesChartElement processes = this.createListProcess();
		
		//objeto que ir� representar o gr�fico
		this.gantt = new GanttChart(this.patternDate, processes);
		
		//cria lista de tarefas e seta no gr�fico
		this.createTasks();
		
		CategoryBuilder catBuilder = new CategoryBuilder(this.startProject, this.endProject, this.sdf, this.gantt, this.locale);
		
		//cria categorias com base nas informa��es do projeto e seta
		//resultados no objeto que representa o gr�fico
		catBuilder.createYearCategory();
		catBuilder.createMonthCategory();
		catBuilder.createDayCategory();
		
		this.gantt.setChartPercent();
		
		return this.gantt;		
	}
	
	/**
	 * Configura elementos iniciais para cria��o do objeto
	 * 
	 */
	private void setupGeneralInformation() {
		
		//ordenamos lista de tarefas por data de execu��o
		Collections.sort(this.project.getTasks(), new TaskDateComparator());
		
		//com a lista ordenada podemos pegar a data inicial
		//do projeto
		this.startProject = (Calendar) this.project.getFirstTaskDate();		
		
		//pegamos o padr�o de data com base no locale enviado
		this.patternDate = DateUtil.getPatternDate(this.locale);
		
		//criamos formatador de datas com base no locale mais
		//o padr�o de horas utilizado pelo gr�fico
		this.sdf = this.getFormat("");
	}

	/**
	 * Valida informa��es enviadas 
	 */
	private void verifyInfos() {
		//se projeto for inv�lido lan�a exception
		if(!Validation.isValid(this.project)){
			throw new IllegalArgumentException("project.null");
		} else if(!Validation.isValid(this.locale)){
			throw new IllegalArgumentException("locale.null");
		}
	}

	/**
	 * Configura lista de tarefas que comp�e o projeto
	 */
	private void createTasks() {
		
		//o gr�fico possui um bug em rela��o a primeira tarefa
		//caso a mesma seja uma transi��o entre anos
	    //portanto setamos o pattern sem data para a primeira
		//tarefa
		//boolean isShowTime = false;
		
		//setamos as tarefas no gr�fico
		for(GanttTask ganttTask : this.project.getTasks()){
						
			String start = DateUtil.CalendarToStr(this.sdf, ganttTask.getPlannedStartDate());
			String end   = DateUtil.CalendarToStr(this.sdf, ganttTask.getPlannedEndDate());
			
			TaskChartElement task = new TaskChartElement(start, end, ganttTask.getId(), ganttTask.getName(), ganttTask.isShowName());			
			this.gantt.addTask(task);
			
			//verifica se a data final da tarefa ultrapassa a data final
			//atualmente setada, em caso positivo atualiza data final
			this.determineEndProject(ganttTask.getPlannedEndDate());
			
/*			//adicionamos a hora para as demais tarefas
			if(!isShowTime){
				this.sdf = this.getFormat(" hh:mm:ss");
				isShowTime = true;
			}
*/		}
	}
	
	/**
	 * Atualiza data final do projeto com base
	 * na data par�metro 
	 * 
	 * @param atualEndDate
	 */
	private void determineEndProject(Calendar atualEndDate) {
		//se ainda n�o possui data final, setamos a atual
		if(!Validation.isValid(this.endProject)){
			this.endProject = (Calendar) atualEndDate.clone();
		
		  //se a data atual for posterior a data utilizada
		  //ent�o atualizamos a data utilizada
		} else if(atualEndDate.after(this.endProject)){
			this.endProject = (Calendar) atualEndDate.clone();
		}
	}

	/**
	 * Cria formatador para as datas
	 * 
	 * @param locale
	 * @return
	 */
	private SimpleDateFormat getFormat(String addFormat) {		
		//criamos um formatador de datas com base nas informa��es
		//descobertas no locale
		return new SimpleDateFormat(this.patternDate+addFormat);
	}

	/**
	 * Configura lista de processos com base nas informa��es do projeto
	 * enviado
	 * 
	 * @param projeto
	 * @return
	 */
	private final ProcessesChartElement createListProcess() {
		
		ProcessesChartElement processes = new ProcessesChartElement(this.project.getName());
		
		//setamos as tarefas como processos do gr�fico
		for(GanttTask ganttTask : this.project.getTasks()){
			ProcessChartElement p = new ProcessChartElement(ganttTask.getId(), ganttTask.getName());			
			processes.addProcess(p);
		}
		
		return processes;
	}
}
