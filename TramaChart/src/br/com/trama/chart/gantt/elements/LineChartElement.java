package br.com.trama.chart.gantt.elements;


/**
 * @author       $h@rk
 * @Date         20/06/2011
 * @project      jotdown-web
 * @Description: Classe que representa �reas demarcadas no gr�fico
 */
class LineChartElement {
	
	private String start;
	private String end;
	private String displayValue;
	private String isTrendZone; 
	private String alpha; 
	private String color;
	private String thickness;
	
	/**
	 * 
	 * @param start        Data inicial de demarca��o (pattern + HH:MM:SS)
	 * @param end          Data final   de demarca��o (pattern + HH:MM:SS)
	 * 
	 * @param displayValue Texto de exibi��o para o trecho demarcado
	 * 
	 * @param isTrendZone  Se true determina que o trecho demarcado
	 *                     compreende o start + date
	 *                     Se falso desenha linha apenas em start
	 *                     
	 * @param alpha        Fator de transpar�ncia do trecho demarcado
	 * 
	 * @param color        Cor de demarca��o
	 * 
	 * @param thickness   Determina espessura da linha desenhada (em pixels)
	 */
	LineChartElement(String start, String end, String displayValue, boolean isTrendZone, int alpha, String color, int thickness) {
		this.start = start;
		this.end = end;
		this.displayValue = displayValue;
		this.isTrendZone = isTrendZone ? "1" : "0";
		this.alpha = ""+alpha;
		this.color = color;
		this.thickness = ""+thickness;
	}
	
	public String getStart() {
		return start;
	}
	public String getEnd() {
		return end;
	}
	public String getDisplayValue() {
		return displayValue;
	}
	public String getColor() {
		return color;
	}

	public String getIsTrendZone() {
		return isTrendZone;
	}

	public String getAlpha() {
		return alpha;
	}

	public String getThickness() {
		return thickness;
	}
}
