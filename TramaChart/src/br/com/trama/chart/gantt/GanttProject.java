package br.com.trama.chart.gantt;

import java.util.Calendar;
import java.util.List;

/**
 * @author       $h@rk
 * @Date         23/06/2011
 * @project      TramaChart
 * @Description: Interface que define os dados de implementação de um gráfico de gantt
 */
public interface GanttProject {

	List<GanttTask> getTasks();

	String getName();

	Calendar getFirstTaskDate();
}
