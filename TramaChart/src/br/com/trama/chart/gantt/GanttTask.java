package br.com.trama.chart.gantt;

import java.util.Calendar;

/**
 * @author       $h@rk
 * @Date         23/06/2011
 * @project      TramaChart
 * @Description: 
 */
public interface GanttTask {

	/**
	 * data inicial prevista
	 * @return
	 */
	Calendar getPlannedStartDate();

	/**
	 * data inicial executada
	 * @return
	 */
	Calendar getPlannedEndDate();
	
	/**
	 * data final prevista
	 * @return
	 */
	Calendar getRunStartDate();

	/**
	 * data final executada
	 * @return
	 */
	Calendar getRunEndDate();
	
	/**
	 * 
	 * @return
	 */
	Long getId();

	String getName();
	
	/**
	 * Determina exibi��o do nome na
	 * barra de tarefa
	 * @return
	 */
	boolean isShowName();	
}
