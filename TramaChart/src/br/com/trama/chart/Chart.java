package br.com.trama.chart;

public interface Chart {
	
	/**
	 * Retorna n�mero de elementos no
	 * eixo x do gr�fico
	 * @return
	 */
	public int getX();
	
	/**
	 * Retorna n�mero de elementos no
	 * eixo y
	 * 
	 * @return
	 */
	public int getY();
	
}
