package br.com.trama.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.trama.chart.gantt.GanttProject;
import br.com.trama.chart.gantt.GanttTask;

public class ProjectImp implements GanttProject{
	
	private String name;
	private List<GanttTask> listTask;
	
	public ProjectImp(String name){
		this.name = name;
		this.listTask = new ArrayList<GanttTask>();
	}

	@Override
	public List<GanttTask> getTasks() {
		// TODO Auto-generated method stub
		return this.listTask;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.name;
	}

	@Override
	public Calendar getFirstTaskDate() {
		return this.listTask.get(0).getPlannedStartDate();
	}

	public void setListTasks(List<GanttTask> tasks) {
		this.listTask = tasks;
	}
}
