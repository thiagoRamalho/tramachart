package br.com.trama.test;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;

import br.com.trama.chart.Chart;
import br.com.trama.chart.gantt.GanttTask;
import br.com.trama.chart.gantt.elements.GanttBuilder;
import br.com.trama.chart.gantt.elements.GanttChart;

import com.thoughtworks.xstream.XStream;

public class GantChartTest {

	private ProjectImp p;

	@Before
	public void setUp() throws Exception {		
		p = new ProjectImp("teste");		
	}
	
	@Test
	public void testGanttViradaAno(){
		
		TaskImp t1 = new TaskImp("tarefa1", new Long(1));
		t1.setPlannedStartDate(new GregorianCalendar(2009, Calendar.DECEMBER, 18));
		t1.setplannedEndDate  (new GregorianCalendar(2009, Calendar.DECEMBER, 29));
		
		TaskImp t2 = new TaskImp("tarefa2", new Long(2));
		
		t2.setPlannedStartDate(new GregorianCalendar(2010, Calendar.JANUARY,  18));
		t2.setplannedEndDate  (new GregorianCalendar(2010, Calendar.FEBRUARY, 28));
		
		TaskImp t3 = new TaskImp("tarefa2", new Long(3));
		
		t3.setPlannedStartDate(new GregorianCalendar(2010, Calendar.DECEMBER, 18));
		t3.setplannedEndDate  (new GregorianCalendar(2011, Calendar.FEBRUARY, 28));
		
		List<GanttTask> tarefas = new ArrayList<GanttTask>();
		tarefas.add(t1);
		tarefas.add(t2);
		tarefas.add(t3);
		p.setListTasks(tarefas);
		
		this.createChart();
	}
	
	@Test
	public void testGanttMesmoAno(){
		
		TaskImp t1 = new TaskImp("tarefa1", new Long(1));
		t1.setPlannedStartDate(new GregorianCalendar(2011, Calendar.JANUARY, 18));
		t1.setplannedEndDate  (new GregorianCalendar(2011, Calendar.FEBRUARY, 29));
		
		TaskImp t2 = new TaskImp("tarefa2", new Long(2));
		
		t2.setPlannedStartDate(new GregorianCalendar(2011, Calendar.MARCH,     18));
		t2.setplannedEndDate  (new GregorianCalendar(2011, Calendar.SEPTEMBER, 28));
		
		TaskImp t3 = new TaskImp("tarefa3", new Long(3));
		
		t3.setPlannedStartDate(new GregorianCalendar(2011, Calendar.NOVEMBER, 18));
		t3.setplannedEndDate  (new GregorianCalendar(2011, Calendar.DECEMBER, 28));
		
		List<GanttTask> tarefas = new ArrayList<GanttTask>();
		tarefas.add(t1);
		tarefas.add(t2);
		tarefas.add(t3);
		p.setListTasks(tarefas);
		
		this.createChart();
	}
	
	@Test
	public void testBugVraptor(){
		
		TaskImp t1 = new TaskImp("tarefa1", new Long(1));
		t1.setPlannedStartDate(new GregorianCalendar(2010, Calendar.DECEMBER, 15));
		t1.setplannedEndDate  (new GregorianCalendar(2010, Calendar.DECEMBER, 25));
		
		TaskImp t2 = new TaskImp("tarefa2", new Long(2));
		
		t2.setPlannedStartDate(new GregorianCalendar(2011, Calendar.JANUARY,  10));
		t2.setplannedEndDate  (new GregorianCalendar(2011, Calendar.FEBRUARY, 20));
		
		List<GanttTask> tarefas = new ArrayList<GanttTask>();
		tarefas.add(t1);
		tarefas.add(t2);
		p.setListTasks(tarefas);
		
		this.createChart();
	}	

	
	@Test
	public void testGanttMesmoAnoViradaMes(){
		
		TaskImp t1 = new TaskImp("tarefa1", new Long(1));
		t1.setPlannedStartDate(new GregorianCalendar(2011, Calendar.JANUARY, 18));
		t1.setplannedEndDate  (new GregorianCalendar(2011, Calendar.FEBRUARY, 28));
		
		List<GanttTask> tarefas = new ArrayList<GanttTask>();
		tarefas.add(t1);
		p.setListTasks(tarefas);
		
		this.createChart();
	}	
	
	@Test
	public void testGanttMesmoAnoMesmoMes(){
		
		TaskImp t1 = new TaskImp("tarefa1", new Long(1));
		t1.setPlannedStartDate(new GregorianCalendar(2011, Calendar.FEBRUARY, 18));
		t1.setplannedEndDate  (new GregorianCalendar(2011, Calendar.FEBRUARY, 28));
		
		List<GanttTask> tarefas = new ArrayList<GanttTask>();
		tarefas.add(t1);
		p.setListTasks(tarefas);
		
		this.createChart();
	}	
	
	@Test
	public void testGanttMesmoAnoMesmoMesMesmoDia(){
		
		TaskImp t1 = new TaskImp("tarefa1", new Long(1));
		t1.setPlannedStartDate(new GregorianCalendar(2011, Calendar.FEBRUARY, 18));
		t1.setplannedEndDate  (new GregorianCalendar(2011, Calendar.FEBRUARY, 18));
		
		List<GanttTask> tarefas = new ArrayList<GanttTask>();
		tarefas.add(t1);
		p.setListTasks(tarefas);
		
		this.createChart();
	}	
	
	@Test
	public void testOrdenacao(){
		
		TaskImp t1 = new TaskImp("tarefa1", new Long(1));
		t1.setPlannedStartDate(new GregorianCalendar(2011, Calendar.JULY, 10));
		t1.setplannedEndDate  (new GregorianCalendar(2011, Calendar.JULY, 31));
		
		TaskImp t2 = new TaskImp("tarefa1", new Long(2));
		t2.setPlannedStartDate(new GregorianCalendar(2011, Calendar.JULY, 01));
		t2.setplannedEndDate  (new GregorianCalendar(2011, Calendar.AUGUST, 02));
		
		List<GanttTask> tarefas = new ArrayList<GanttTask>();
		tarefas.add(t1);
		tarefas.add(t2);
		p.setListTasks(tarefas);
		
		this.createChart();
	}	
	
	@Test
	public void bugSQLServer(){
		
		TaskImp t1 = new TaskImp("tarefa1", new Long(1));
		t1.setPlannedStartDate(new GregorianCalendar(2011, Calendar.AUGUST, 06));
		t1.setplannedEndDate  (new GregorianCalendar(2011, Calendar.AUGUST, 06));
		
		TaskImp t2 = new TaskImp("tarefa2", new Long(2));
		t2.setPlannedStartDate(new GregorianCalendar(2011, Calendar.AUGUST, 07));
		t2.setplannedEndDate  (new GregorianCalendar(2011, Calendar.AUGUST, 07));
		
		List<GanttTask> tarefas = new ArrayList<GanttTask>();
		tarefas.add(t1);
		tarefas.add(t2);
		p.setListTasks(tarefas);
		
		this.createChart();
	}	

	

	private void createChart() {
		GanttBuilder b = new GanttBuilder(this.p, Locale.getDefault());
		Chart chart = b.createChart();
		
		XStream xs = new XStream();
		//mudar visibilidade da classe para rodar
		//esse teste
		xs.processAnnotations(GanttChart.class);
		String xml = xs.toXML(chart);
		xml = xml.replace("\"", "'");
		System.out.println(xml);
		System.out.println("xAxys "+chart.getX());
		System.out.println("yAxis "+chart.getY());
	}
	
}
