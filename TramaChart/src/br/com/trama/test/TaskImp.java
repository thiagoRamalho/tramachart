package br.com.trama.test;

import java.util.Calendar;

import br.com.trama.chart.gantt.GanttTask;

/**
 * @author       $h@rk
 * @Date         24/06/2011
 * @project      TramaChart
 * @Description: 
 */
public class TaskImp implements GanttTask{

	private String name;
	private Long id;
	private Calendar plannedStartDate;
	private Calendar plannedEndDate;

	public TaskImp(String name, Long id) {
		this.name = name;
		this.id = id;
	}

	@Override
	public Calendar getPlannedStartDate() {
		// TODO Auto-generated method stub
		return this.plannedStartDate;
	}

	@Override
	public Calendar getPlannedEndDate() {
		// TODO Auto-generated method stub
		return this.plannedEndDate;
	}

	@Override
	public Calendar getRunStartDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Calendar getRunEndDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return this.id;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.name;
	}

	@Override
	public boolean isShowName() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setPlannedStartDate(Calendar plannedStartDate) {
		this.plannedStartDate = plannedStartDate;
	}

	public void setplannedEndDate(Calendar plannedEndDate) {
		this.plannedEndDate = plannedEndDate;
	}

}
